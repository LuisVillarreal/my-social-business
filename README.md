# My social business

para crear propuestas

Nada de lo que hice antes funciona

    - quiero hacer una lista
    - tener un mmento de reflexión
*italica*
**negrita**
---
title: 'Taller #3- Regresión lineal'
author: "Pablo Leautaud-Valenzuela y Juan David Mejía- Grupo B"
date: "Septiembre 15 de 2019"
output:
  html_document : default
---
```{r, include=FALSE}
options(tinytex.verbose = TRUE)
```

## Objetivos Generales:

1. Utilizar el método de mínimos cuadrados para ajustar una línea a los datos mediante la obtención de:
  a) Suma de cuadrados X (SCX)
  b) Suma de cuadrados Y (SCY)
  c) Suma del producto XY (SPXY)
  d) Suma de cuadrados del error (SCE)
2. Estimar los coeficientes b0 y b1
3.  Presentar la ecuación completa
4.  Obtener el coeficiente de determinación R2
5.  Graficar los datos originales y la línea de ajuste (con los valores calculados)
6.  Graficar los valores predichos vs los residuales (OJO: con los valores calculados, no la generada en excel)

OJO: Utilizar dos decimales en los cálculos

### Paquetes necesarios para el script:
```{r eval=TRUE}
library(rmarkdown)
library(knitr)
library(markdown)
library(lattice)
library(ggplot2)
library(kableExtra)
```

## Parte 1: Regresión lineal simple

Determinar el poder predictivo de la ecuación lineal. Determinar el porcentaje de variabilidad en la extensión de hielo ártico (millones de $km^2$) explicado por la concentración de $CO_2$ atmosférico (ppm).

```{r eval=TRUE}
concentracion <- c(310, 314.21, 318.42, 322.63, 326.84, 331.05, 335.26, 339.47, 343.68, 347.89)
extension_hielo <- c(9, 8.7, 7.5, 7.6, 6.5, 7, 6.9, 5.8, 5.6, 6.9)
datos <- data.frame("CO2" = concentracion, "Extensión hielo ártico" = extension_hielo)
head(datos)
kable(datos) %>%
  kable_styling("striped", full_width = F) %>%
  row_spec(0, angle = 0)
names(datos)[names(datos) == "CO2"] <- "X"
names(datos)[names(datos) == "Extensión.hielo.ártico"] <- "Y"
```

#### Definición de funciones para el cálculo estadístico descriptivo:

##### Media aritmética
```{r eval=TRUE}
media <- function(x) sum(x)/length(x)
```
##### Cálculo de las medias para X y Y
```{r eval=TRUE}
medias <- round((apply(datos[1:2], 2, media)), digits = 2)
medias
mediax <- medias[1]
mediax
mediay <- medias[2]
mediay
```

### a) Tabla con todas las entradas:

Con la función *within()*, se agregan nuevas columnas al dataframe, a continuación se generan las columnas $x_{i}- \bar{x}$, $(x_{i}- \bar{x})^2$, $y_{i}- \bar{y}$, $(y_{i}- \bar{y})^2$ y $PXY = (x_{i}- \bar{x})*(y_{i}- \bar{y})$

```{r eval=TRUE}
C <- within(datos, {
  ximenosmedia <- round(((X - mediax)), digits = 2)
  ximenosmedia2 <- round((ximenosmedia^2), digits = 2)
  yimenosmedia <- round(((Y - mediay)), digits = 2)
  yimenosmedia2 <- round((yimenosmedia^2), digits = 2)
  PXY <- round((ximenosmedia*yimenosmedia), digits = 2)
  })
C <- C[c(1,2,7,6,5,4,3)]
kable(C) %>%
  kable_styling("striped", full_width = F) %>%
  row_spec(0, angle = 0)
```

### b) Cálculo de SCX, SCY y SPXY:

```{r eval=TRUE}
SCX <- sum(C$ximenosmedia2)
SCX
SCY <- sum(C$yimenosmedia2)
SCY
SPXY <- sum(C$PXY)
SPXY
```

### c) Cálculo de bo, bi y ecuación final:
```{r eval=TRUE}
b1 <- round((SPXY/SCX), digits = 2)
b1
```  
Para calcular las constantes de la función $y(x) = b_{0}+b_{0}x$, se realizan las siguientes operaciones:
$$b_{1}= \frac{SPXY}{SCX}=\frac{-106.73}{1462.23}$$
$$b_{1}=-0.07$$
```{r eval=TRUE}
bcero <- round((mediay - (b1*mediax)), digits = 2)
bcero
```  
$$b_{0}= \bar{y}-(b_{1}*\bar{x})= 7.15 -(-0.07*328.94)$$
$$b_{0}=30.18$$
Finalmente, la función se escribe cómo:

$$y(x) = 30.18-0.07x$$
La regresión presenta una pendiente negativa, indicando una relación inversa entre la variable independiente (Concentración de $CO_2$) y la dependiente (Extensión de hielo ártico). Por lo anterior, se puede afirmar que por cada incremento de 1 parte por millón en la concentración de $CO_2$ en la atmósfera, se disminuye la superficie de hielo ártico en 700.000 $km^2$.

```{r eval=TRUE}
funcion <- function(x) 30.18 - 0.07*x 
```

### d) Una tabla que contenga todas las entradas de: ŷ, yi-ŷ, (yi-ŷ)2 correspondiente para la obtención de SCE.

```{r eval=TRUE}
CDOS <- within(datos, {
  ypred <- round((apply(datos[1], 2, funcion)), digits = 2)
  dify <- round((Y-ypred), digits = 2)
  dify2 <- round((dify^2), digits = 2)
  })
CDOS <- CDOS[c(1,2,5,4,3)]
kable(CDOS) %>%
  kable_styling("striped", full_width = F) %>%
  row_spec(0, angle = 0)
```
El valor de $SCE$ se obtiene con $\sum(yi-ŷ)^2$ de la última columna de la tabla anterior.
```{r eval=TRUE}
SCE <- sum(CDOS$dify2)
SCE
```

### e) Cálculo de R2.

Con los valores calculados anteriormente, se calcula el valor de $SCR$ y $R2$, así:
```{r eval=TRUE}
SCR <- SCY - SCE
SCR
```
$$SCR = SCY - SCE= 10.92 - 3.15= 7.77$$
Y
```{r eval=TRUE}
RDOS <- round((SCR/SCY), digits = 2)
RDOS
```
$$R^2=\frac{SCR}{SCY}=0.71$$
Por lo anterior, la variabilidad de la concentración de $CO_{2}$, es capaz de explicar el $71\%$ de la variabilidad observada en la extensión del hielo ártico.

#### Validación con regresión lineal en R.
```{r eval=TRUE}
modelo_simple <- lm(data = datos,formula = Y ~ X)
summary(modelo_simple)
```

#### Evaluar la significancia de la regresión lineal.

La hipótesis nula $H_0:$ Regresión lineal NO es significativa
La hipótesis alternativa $H_1:$ Regresión lineal SI es significativa

Con el valor estadístico resultante de $F=19.75$, y dado que el valor $p= 0.0021$ es menor al nivel de significancia $\alpha=0.05$, se puede rechazar la Hipótesis nula$H_0:$ y concluír que la regresión líneal es estadísticamente significativa.



### f) Gráfica de dispersión mostrando la línea de ajuste.

```{r eval=TRUE}
linea <- as.vector(funcion(datos$X))
plot(datos$X, datos$Y, main = "Diagráma de dispersión",
     col = "blue4",
     lwd = 3,
     xlab = "Concentración de CO2 atmosférico (ppm)", 
     ylab = "Extensión de hielo ártico (millones de km2)"
     )
lines(datos$X, linea, col= "red4", lwd =1.5)
text(330, 8, "y(x)=30.18 - 0.07x", col = "red4")
```

### g) Gráfica de residuales
```{r eval=TRUE}
plot(CDOS$ypred, CDOS$dify, main = "Gráfica de residuales",
     col = "blue4",
     lwd = 3,
     xlab = "Valores ajustados", 
     ylab = "Residuales"
     )
abline(h = 0, col= "red4", lwd =1.5)
```

## Parte 2. Regresión lineal múltiple en R. ¿Varía el coeficiente de determinación con respecto a la regresión lineal simple? Especificar la ecuación final y determinar si la regresión es significativa.

```{r eval=TRUE}
temperatura <- c(20, 19, 22, 21, 22, 23, 24, 25, 26, 27)
datos1 <- data.frame(datos$Y, datos$X, temperatura)
colnames(datos1)
names(datos1)[names(datos1) == "datos.X"] <- "CO2"
names(datos1)[names(datos1) == "datos.Y"] <- "Hielo ártico"
kable(datos1) %>%
  kable_styling("striped", full_width = F) %>%
  row_spec(0, angle = 0)
```
#### Diagramas de dispersión para intuir comportamiento de las variables

```{r eval=TRUE}
par(mfrow = c(1,2))
plot(datos1$CO2, datos1$`Hielo ártico`, main = "Diagráma de dispersión 1",
     col = "red4",
     lwd = 3,
     xlab = "Concentración de CO2 atmosférico (ppm)", 
     ylab = "Extensión de hielo ártico (millones de km2)"
     )
plot(datos1$temperatura, datos1$`Hielo ártico`, main = "Diagráma de dispersión 2",
     col = "red4",
     lwd = 3,
     xlab = "Temperatura (ºC)", 
     ylab = "Extensión de hielo ártico (millones de km2)"
     )
```

#### Obtención de información de regresión lineal múltiple mediante función lm()

```{r eval=TRUE}
modelo_multiple <- lm(formula = datos1$`Hielo ártico` ~ datos1$CO2+datos1$temperatura, data = datos1)
summary(modelo_multiple)
```

#### ¿Varía el coeficiente de determinación con respecto a la regresión lineal simple?

No, a pesar de que se están contemplando dos variables como determinantes, el valor obtenido para $R^2$ sigue siendo $0.71$. lo anterior puede indicar que la variable adicional de la temperatura no tiene un peso significativo en la regresión.

#### Ecuación final

Sustituyendo los valores de los coeficientes en la ecuación obtenemos:

$$ y=32.1826 - 0.0778*Nivel de CO_{2}+ 0.0244*Temperatura $$

Por lo anterior, la variabilidad de la concentración de $CO_{2}$ y de la temperatura, son capaces de explicar el $71\%$ de la variabilidad observada en la extensión del hielo ártico.

### Gráficas de dispersión mostrando la línea de ajuste.

```{r eval=TRUE}
ggplot(datos1, aes(x=datos1$CO2, y=datos1$`Hielo ártico`)) +
  geom_point(shape=1) +    # genera circulos
  geom_smooth(method=lm)# Genera sombra gris
```

```{r eval=TRUE}
ggplot(datos1, aes(x=datos1$temperatura, y=datos1$`Hielo ártico`)) +
  geom_point(shape=1) +    # genera circulos
  geom_smooth(method=lm) # Genera sombra gris
```

### Gráfica de residuales.


```{r eval=TRUE}
par(mfrow = c(1,2))
plot(modelo_multiple)